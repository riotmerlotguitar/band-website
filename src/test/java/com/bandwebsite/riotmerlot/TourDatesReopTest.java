package com.bandwebsite.riotmerlot;

import com.bandwebsite.riotmerlot.Model.TourDates;
import com.bandwebsite.riotmerlot.Repository.TourDatesRepo;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=RiotMerlotTestConfig.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class TourDatesReopTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TourDatesRepo tourRepo;

    private List<TourDates> expectedDates;
    private List<TourDates> receivedDates;
    private TourDates td1, td2, td3, td4;

    @Before
    public void setUp() {
        expectedDates = new ArrayList<>();
        receivedDates = new ArrayList<>();

        entityManager.clear();

        td1 = new TourDates();
        td1.setTourDate(new Date(2019, 4, 12));
        td1.setCountry("United States");
        td1.setState("California");
        td1.setCity("Hollywood");
        td1.setVenue("House of Blues");
        td1.setPrice(56);
        td1 = entityManager.persistAndFlush(td1);

        td2 = new TourDates();
        td2.setTourDate(new Date(2019, 4, 14));
        td2.setCountry("United States");
        td2.setState("California");
        td2.setCity("Ventura");
        td2.setVenue("The Majestic");
        td2.setPrice(35);
        td2 = entityManager.persistAndFlush(td2);

        td3 = new TourDates();
        td3.setTourDate(new Date(2019, 5, 2));
        td3.setCountry("Canada");
        td3.setState("Manitoba");
        td3.setCity("Winnipeg");
        td3.setVenue("The Drunken Mountie");
        td3.setPrice(15);
        td3 = entityManager.persistAndFlush(td3);

        td4 = new TourDates();
        td4.setTourDate(new Date(2019, 5, 6));
        td4.setCountry("Canada");
        td4.setState("Ontario");
        td4.setCity("Toronto");
        td4.setVenue("The Yard Sale");
        td4.setPrice(15);
        td4 = entityManager.persistAndFlush(td4);
    }

    @AfterEach
    public void clearTestData() {
        expectedDates.clear();
        receivedDates.clear();
    }

    @Test()
    public void testCount() {
        receivedDates = (List<TourDates>)tourRepo.findAll();
        assertThat(receivedDates.size()).isEqualTo(4);
    }

    @Test()
    public void findDatesByCountryTest() throws Exception {
        expectedDates.add(td3);
        expectedDates.add(td4);

        receivedDates = (List<TourDates>)tourRepo.findAllByCountry("Canada");
        assertThat(receivedDates).isEqualTo(expectedDates);
    }

    @Test
    public void findAllCurrentTourDates() throws Exception {
        expectedDates.add(td3);
        expectedDates.add(td4);

        receivedDates = (List<TourDates>)tourRepo.findAllCurrentTourDates();
        System.out.println("****************  List Output **********");
        for (TourDates td : receivedDates) {
            System.out.println(td.toString());
            System.out.println(td.getCity());
        }
        System.out.print("**********************************");

        assertThat(receivedDates).isEqualTo(expectedDates);
    }

//    @Test()
//    public void givenParentId_whenRetrieving_thenReturnAllChildRefCodes() {
//        RefCode r1 = new RefCode();
//        r1.setName("Pretend Parent #1");
//        r1 = entityManager.persistAndFlush(r1);
//
//        RefCode r2 = new RefCode();
//        r2.setName("Child #1");
//        r2.setParentId(r1.getId());
//        r2 = entityManager.persistAndFlush(r2);
//
//        RefCode r3 = new RefCode();
//        r3.setName("Child #2");
//        r3.setParentId(r1.getId());
//        r3 = entityManager.persistAndFlush(r3);
//
//        RefCode r4 = new RefCode();
//        r4.setName("Parent #2");
//        r4 = entityManager.persistAndFlush(r4);
//
//        List<RefCode> parent1Children = refRepo.findByParentId(1L);
//
//        assertThat(parent1Children).isNotNull();
//        assertThat(parent1Children.size()).isEqualTo(2);
//        assertThat(parent1Children.get(0).getName()).isEqualToIgnoringCase("Child #1");
//        assertThat(parent1Children.get(1).getName()).isEqualToIgnoringCase("Child #2");
//
//        List<RefCode> parent2Children = refRepo.findByParentId(4L);
//
//        assertThat(parent2Children.size()).isEqualTo(0);
//
//        List<RefCode> allRefCodes = (List<RefCode>) refRepo.findAll();
//        assertThat(allRefCodes.get(3).getName()).isEqualToIgnoringCase("Parent #2");
//    }

}
