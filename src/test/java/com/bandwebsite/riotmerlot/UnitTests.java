package com.bandwebsite.riotmerlot;

import com.bandwebsite.riotmerlot.Controller.RiotController;
import com.bandwebsite.riotmerlot.Model.Check;
import com.bandwebsite.riotmerlot.Repository.AlbumInfoRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.InvalidParameterException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class UnitTests {
//    @Test()
//    public void album() {
//        public final AlbumInfoRepo albumRepo;
//
//        RiotController ck = new getAlbums(partySize, checkTotal);
//
//        double finalTotal = ck.calculateTotal();
//        assertThat(finalTotal).isEqualTo(42);
//    }

    @Test()
    public void given_party_greater_than_or_equal_to_six_when_calculating_total_return_total_with_eighteen_percent_gratuity() {
        double checkTotal = 42;
        int partySize = 7;

        Check ck = new Check(partySize, checkTotal);

        double finalTotal = ck.calculateTotal();
        assertThat(finalTotal).isEqualTo(49.56);
    }

    @Test()
    public void given_party_equal_to_six_when_calculating_total_return_total_with_eighteen_percent_gratuity() {
        double checkTotal = 42;
        int partySize = 6;

        Check ck = new Check(partySize, checkTotal);

        double finalTotal = ck.calculateTotal();
        assertThat(finalTotal).isEqualTo(49.56);
    }

    @Test(expected = InvalidParameterException.class)
    public void given_party_is_negative_when_calculating_total_return_total_with_eighteen_percent_gratuity() {
        double checkTotal = 42;
        int partySize = -1;

        Check ck = new Check(partySize, checkTotal);
    }

    @Test()
    public void given_any_check_with_any_set_of_parameters_when_calculating_total_return_total_with_fifteen_percent_gratuity() {
        double checkTotal = 42;
        int partySize = 5;

        Check ck = new Check(partySize, checkTotal);

        double fifteenPercent = ck.caclulateFifteeenPercentTip();
        assertThat(fifteenPercent).isEqualTo(6.30);
    }

    @Test()
    public void given_any_check_with_any_set_of_parameters_when_calculating_total_return_total_with_eighteen_percent_gratuity() {
        double checkTotal = 42;
        int partySize = 5;

        Check ck = new Check(partySize, checkTotal);

        double eighteenPercent = ck.caclulateEighteenPercentTip();
        assertThat(eighteenPercent).isEqualTo(7.56);
    }

    @Test()
    public void given_any_check_with_any_set_of_parameters_when_calculating_total_return_total_with_twenty_percent_gratuity() {
        double checkTotal = 42;
        int partySize = 5;

        Check ck = new Check(partySize, checkTotal);

        double twentyPercent = ck.caclulateTwentyPercentTip();
        assertThat(twentyPercent).isEqualTo(8.40);
    }

}
