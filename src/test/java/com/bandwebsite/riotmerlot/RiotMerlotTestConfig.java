package com.bandwebsite.riotmerlot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Primary;

@SpringBootConfiguration
@EnableAutoConfiguration
public class RiotMerlotTestConfig {
    @Primary
    public static void main(String[] args){
        SpringApplication.run(RiotmerlotApplication.class, args);
    }


}
