package com.bandwebsite.riotmerlot;

import com.bandwebsite.riotmerlot.Controller.RiotController;
import com.bandwebsite.riotmerlot.Model.AlbumInfo;
import com.bandwebsite.riotmerlot.Model.BandInfo;
import com.bandwebsite.riotmerlot.Model.Media;
import com.bandwebsite.riotmerlot.Model.TourDates;
import com.bandwebsite.riotmerlot.Services.RiotControllerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;
import java.sql.Date;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@WebMvcTest(value = RiotController.class)
@ContextConfiguration(classes=RiotMerlotTestConfig.class)
public class RiotControllerTest {
    @Autowired
    private MockMvc mockMVC;

    @InjectMocks
    private RiotController controller;

    @MockBean
    private RiotControllerService controllerTestService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockMVC = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void getAlbumsTest() throws Exception {
        List<AlbumInfo> albumList = new ArrayList<>();
        AlbumInfo album = new AlbumInfo();

        album.setName("Divine Conspiracy");
        album.setDescription("Bitchin album.");
        album.setImage("/assets/img1");
        album.setReleaseDate(new Date(2010, 11, 12));
        albumList.add(album);

        AlbumInfo album2 = new AlbumInfo();
        album2.setName("The Phantom Agony");
        album2.setDescription("First Epica Album, I think.");
        album2.setImage("/assets/img2");
        album2.setReleaseDate(new Date(2002, 5, 3));
        albumList.add(album2);

        Mockito.when(controllerTestService.getAlbums()).thenReturn(albumList);

        String URL = "/albums";
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URL).accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMVC.perform(requestBuilder).andReturn();

        String expectedJSON = this.mapToJson(albumList);
        String resultJson = result.getResponse().getContentAsString();
        assertThat(resultJson).isEqualTo(expectedJSON);
    }

    @Test()
    public void getAllBandInfoTest() throws Exception {
        List<BandInfo> bandList = new ArrayList<>();

        BandInfo bi1 = new BandInfo();
        bi1.setName("Epica");
        bi1.setImage("/img/EpicaImg");
        bi1.setDescription("Formed in The Netherlands, Epica is a Symphonic Metal band with a gorgeous redhead singer");

        BandInfo bi2 = new BandInfo();
        bi2.setName("Eluveitie");
        bi2.setImage("/img/EluveitieImg");
        bi2.setDescription("The Slipknot of Celtic Folk Metal.  They are a fantastic mix of fast and heavy metal with plenty of folk influences.");

        bandList.add(bi1);
        bandList.add(bi2);

        Mockito.when(controllerTestService.getAllBandInfo()).thenReturn(bandList);

        String URL = "/band_info";
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URL).accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMVC.perform(requestBuilder).andReturn();

        String expectedJSON = this.mapToJson(bandList);
        String resultJson = result.getResponse().getContentAsString();
        assertThat(resultJson).isEqualTo(expectedJSON);
    }

    @Test()
    public void getAllMediaTest() throws Exception {
        List<Media> mediaList = new ArrayList<>();

        Media m1 = new Media();
        m1.setTitle("Consign To Oblivian");
        m1.setDescription("One of Epica's first songs.");
        m1.setVideo("/videos/consignToOblivian");

        Media m2 = new Media();
        m2.setTitle("A Song for Epona");
        m2.setDescription("One of Eluveitie's most popular songs.");
        m2.setVideo("/videos/songForEpona");

        mediaList.add(m1);
        mediaList.add(m2);

        Mockito.when(controllerTestService.getAllMedia()).thenReturn(mediaList);

        String URL = "/media";
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URL).accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMVC.perform(requestBuilder).andReturn();

        String expectedJSON = this.mapToJson(mediaList);
        String resultJson = result.getResponse().getContentAsString();
        assertThat(resultJson).isEqualTo(expectedJSON);
    }

    @Test()
    public void getAllTourDatesTest() throws Exception {
        List<TourDates> tourList = new ArrayList<>();

        TourDates td1 = new TourDates();
        td1.setTourDate(new Date(2019, 4, 12));
        td1.setCountry("United States");
        td1.setState("California");
        td1.setCity("Hollywood");
        td1.setVenue("House of Blues");
        td1.setPrice(56);

        TourDates td2 = new TourDates();
        td2.setTourDate(new Date(2019, 4, 14));
        td2.setCountry("United States");
        td2.setState("California");
        td2.setCity("Ventura");
        td2.setVenue("The Majestic");
        td2.setPrice(35);

        tourList.add(td1);
        tourList.add(td2);

        Mockito.when(controllerTestService.getAllTourDates()).thenReturn(tourList);

        String URL = "/tour_dates";
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URL).accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMVC.perform(requestBuilder).andReturn();

        String expectedJSON = this.mapToJson(tourList);
        String resultJson = result.getResponse().getContentAsString();
        assertThat(resultJson).isEqualTo(expectedJSON);
    }


    private String mapToJson (Object ob) throws IOException {
        ObjectMapper om = new ObjectMapper();

        return om.writeValueAsString(ob);
    }
}
