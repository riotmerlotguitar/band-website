package com.bandwebsite.riotmerlot.Service;

import com.bandwebsite.riotmerlot.Model.AlbumInfo;
import com.bandwebsite.riotmerlot.Repository.AlbumInfoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AlbumInfoTestService {
    @Autowired
    private AlbumInfoRepo albumRepo;
    public Iterable<AlbumInfo> getAllAlbums() {
        return albumRepo.findAll();
    }
}
