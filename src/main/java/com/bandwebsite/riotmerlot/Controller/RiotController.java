package com.bandwebsite.riotmerlot.Controller;

import com.bandwebsite.riotmerlot.Model.*;
import com.bandwebsite.riotmerlot.Repository.*;
import com.bandwebsite.riotmerlot.Services.RiotControllerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class RiotController
{

    // ***** Member Variables *****
    @Autowired
    private final AlbumInfoRepo albumRepo;

    @Autowired
    private final BandInfoRepo bandRepo;

    @Autowired
    private final MediaRepo mediaRepo;

    @Autowired
    private final MerchandiseCategoryRepo merchandiseCategoryRepo;

    @Autowired
    private final NewsRepo newsRepo;

    @Autowired
    private final TourDatesRepo tourDatesRepo;

    @Autowired
    private final TrackInfoRepo trackInfoRepo;

    @Autowired
    private RiotControllerService riotService;


    // ***** Constructors *****
    public RiotController(AlbumInfoRepo albumRepo,
                          BandInfoRepo bandRepo,
                          MediaRepo mediaRepo,
                          MerchandiseCategoryRepo merchandiseCategoryRepo,
                          NewsRepo newsRepo,
                          TourDatesRepo tourDatesRepo,
                          TrackInfoRepo trackInfoRepo)
    {
        this.albumRepo = albumRepo;
        this.bandRepo = bandRepo;
        this.mediaRepo = mediaRepo;
        this.merchandiseCategoryRepo = merchandiseCategoryRepo;
        this.newsRepo = newsRepo;
        this.tourDatesRepo = tourDatesRepo;
        this.trackInfoRepo = trackInfoRepo;
    }


    // ***** Functions *****
    @GetMapping(value = "/albums", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AlbumInfo> getAlbums(){
        return (List<AlbumInfo>) riotService.getAlbums();
    }

    @GetMapping(value = "/band_info", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BandInfo> getAllBandInfo() {
        return (List<BandInfo>) this.riotService.getAllBandInfo();
    }

    @GetMapping(value = "/media", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Media> getAllMedia() {
        return (List<Media>) this.riotService.getAllMedia();
    }

    @GetMapping(value = "/tracks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TrackInfo> getTracks(){
        return (List<TrackInfo>) this.riotService.getTracks();
    }

    @GetMapping(value = "/news", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<News> getAllNews() {
        return (List<News>) this.newsRepo.findAll();
    }

    @GetMapping(value = "/tour_dates", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TourDates> getAllTourDates() {
        return (List<TourDates>)this.riotService.getAllTourDates();
    }

    @GetMapping(value = "/tracks/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TrackInfo> getAlbumTracks(@PathVariable Long id) {
        return this.trackInfoRepo.findAllByAlbumId(id);
    }
}
