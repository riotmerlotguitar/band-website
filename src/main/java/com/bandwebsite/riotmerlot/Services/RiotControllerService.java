package com.bandwebsite.riotmerlot.Services;

import com.bandwebsite.riotmerlot.Model.*;
import com.bandwebsite.riotmerlot.Repository.*;
import com.sun.org.apache.xml.internal.dtm.ref.DTMDefaultBaseIterators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RiotControllerService {
    @Autowired
    private AlbumInfoRepo albumRepo;

    @Autowired
    private BandInfoRepo bandrepo;

    @Autowired
    private  MediaRepo mediaRepo;

    @Autowired
    private TourDatesRepo tourRepo;

    @Autowired
    private TrackInfoRepo trackRepo;

    private AlbumInfo album;

    public Iterable<AlbumInfo> getAlbums () {
        return albumRepo.findAll();
    }

    public Iterable<BandInfo> getAllBandInfo () {
        return this.bandrepo.findAll();
    }

    public Iterable<Media> getAllMedia () {
        return this.mediaRepo.findAll();
    }

    public Iterable<TourDates> getAllTourDates() {
        return this.tourRepo.findAll();
    }

    public Iterable<TrackInfo> getTracks() {
        return this.trackRepo.findAll();
    }
}
