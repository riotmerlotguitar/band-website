package com.bandwebsite.riotmerlot.Repository;

import com.bandwebsite.riotmerlot.Model.AlbumInfo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AlbumInfoRepo extends CrudRepository<AlbumInfo, Long> {
   List<AlbumInfo> findAlbumInfoByName(String name);


}
