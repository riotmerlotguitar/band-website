package com.bandwebsite.riotmerlot.Repository;

import com.bandwebsite.riotmerlot.Model.News;
import org.springframework.data.repository.CrudRepository;

public interface NewsRepo extends CrudRepository<News, Long>
{
}
