package com.bandwebsite.riotmerlot.Repository;

import com.bandwebsite.riotmerlot.Model.Media;
import org.springframework.data.repository.CrudRepository;

public interface MediaRepo extends CrudRepository<Media, Long>
{
}
