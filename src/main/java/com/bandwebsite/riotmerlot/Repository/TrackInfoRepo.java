package com.bandwebsite.riotmerlot.Repository;

import com.bandwebsite.riotmerlot.Model.TrackInfo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TrackInfoRepo extends CrudRepository<TrackInfo, Long> {
    List<TrackInfo> findAllByAlbumId(Long id);
}
