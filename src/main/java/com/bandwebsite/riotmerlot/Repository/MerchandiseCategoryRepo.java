package com.bandwebsite.riotmerlot.Repository;

import com.bandwebsite.riotmerlot.Model.MerchandiseCategory;
import org.springframework.data.repository.CrudRepository;

public interface MerchandiseCategoryRepo extends CrudRepository<MerchandiseCategory, Long> {
}
