package com.bandwebsite.riotmerlot.Repository;

import com.bandwebsite.riotmerlot.Model.BandInfo;
import org.springframework.data.repository.CrudRepository;

public interface BandInfoRepo extends CrudRepository<BandInfo, Long>
{

}
