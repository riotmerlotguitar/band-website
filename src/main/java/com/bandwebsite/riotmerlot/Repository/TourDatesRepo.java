package com.bandwebsite.riotmerlot.Repository;

import com.bandwebsite.riotmerlot.Model.TourDates;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface TourDatesRepo extends CrudRepository<TourDates, Long> {

    Iterable<TourDates> findAllByCountry(String country);

    @Query("SELECT td FROM TourDates td WHERE td.tourDate >= CURRENT_DATE")
    Iterable<TourDates> findAllCurrentTourDates();
}
