package com.bandwebsite.riotmerlot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiotmerlotApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(RiotmerlotApplication.class, args);
    }

}
