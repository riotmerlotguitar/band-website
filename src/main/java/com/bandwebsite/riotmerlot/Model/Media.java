package com.bandwebsite.riotmerlot.Model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "media")
@Getter @Setter
@Entity
public class Media
{
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @Column
    private String video;

    @Column
    private String description;
}
