package com.bandwebsite.riotmerlot.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter @Setter
public class Merchandise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;
    @Column
    private float price;
    @Column
    private String image;

    @ManyToOne
    @JoinColumn(name = "categoryID", referencedColumnName = "id")
    private MerchandiseCategory categoryID;

    @ManyToMany(mappedBy = "merchandise")
    @Nullable
    private List<Cart> carts;



}
