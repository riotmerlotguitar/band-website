package com.bandwebsite.riotmerlot.Model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter @Setter
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    @JoinTable(name = "cart_tickets",
    joinColumns = {@JoinColumn(name = "cart_id")},
    inverseJoinColumns = {@JoinColumn(name = "tour_id")})
    private List<TourDates> tickets;

    @ManyToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    @JoinTable(name = "cart_merch",
            joinColumns = {@JoinColumn(name = "cart_id")},
            inverseJoinColumns = {@JoinColumn(name = "merch_id")})
    private List<Merchandise> merchandise;
}
