package com.bandwebsite.riotmerlot.Model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Time;

@Entity
@Table(name="track_info")
@Getter @Setter
public class TrackInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String duration;

    @ManyToOne
    @JoinColumn(name="album", referencedColumnName = "ID")
    private AlbumInfo album;
}
