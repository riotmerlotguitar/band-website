package com.bandwebsite.riotmerlot.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name="tour_dates")
@Getter @Setter
public class TourDates {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private Date tourDate;
    @Column
    private String country;
    @Column
    private String state;
    @Column
    private String city;
    @Column
    private String venue;
    @Column
    private float price;

    @ManyToMany(mappedBy = "tickets")
    @Nullable
    private List<Cart> carts;
}
