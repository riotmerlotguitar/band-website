package com.bandwebsite.riotmerlot.Model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name="album_info")
@Getter @Setter
public class AlbumInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String image;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private Date releaseDate;

    public String printImage()
    {
        return image;
    }
}
