package com.bandwebsite.riotmerlot.Model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "news")
@Getter @Setter
@Entity
public class News
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @Column
    private String image;

    @Column
    private String description;
}
