USE RiotMerlot_db;

CREATE TABLE merchandise_category
(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(32),
  CONSTRAINT pk_category PRIMARY KEY (id)
);

CREATE TABLE tour_dates
(
  id        INT UNSIGNED NOT NULL AUTO_INCREMENT,
  tour_date date,
  country   VARCHAR(64),
  state     VARCHAR(32),
  city      VARCHAR(64),
  venue     VARCHAR(64),
  price     DECIMAL,
  CONSTRAINT pk_tours PRIMARY KEY (id)
);

CREATE TABLE band_info
(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name  VARCHAR(64),
  image VARCHAR(64),
  description VARCHAR(400),
  CONSTRAINT pk_band PRIMARY KEY (id)
);

CREATE TABLE media
(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  title VARCHAR(64),
  video VARCHAR(64),
  description VARCHAR(400),
  CONSTRAINT pk_media PRIMARY KEY (id)
);

CREATE TABLE news
(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  title VARCHAR(100),
  image VARCHAR(64),
  description VARCHAR(400),
  CONSTRAINT pk_news PRIMARY KEY (id)
);

CREATE TABLE album_info
(
  id           INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name         VARCHAR(64),
  description  VARCHAR(200),
  release_date date,
  image        VARCHAR(100),
  CONSTRAINT pk_album PRIMARY KEY (id)
);

CREATE TABLE track_info
(
  id           INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name         VARCHAR(64),
  duration     VARCHAR(64),
  album_id     INT UNSIGNED,
  CONSTRAINT pk_album PRIMARY KEY (id),
  FOREIGN KEY (album_id) REFERENCES album_info(id)
);

CREATE TABLE merchandise
(
  id            INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name          VARCHAR(64),
  price         DECIMAL,
  image         VARCHAR(200),
  category_id   INT UNSIGNED,
  CONSTRAINT pk_merchandise PRIMARY KEY (id),
  FOREIGN KEY (category_id) REFERENCES merchandise_category(id)
);

CREATE TABLE cart
(
  id                INT UNSIGNED NOT NULL AUTO_INCREMENT,
  merchandise_id    INT UNSIGNED,
  tour_id           INT UNSIGNED,
  CONSTRAINT pk_cart PRIMARY KEY (id),
  FOREIGN KEY (merchandise_id) REFERENCES merchandise(id),
  FOREIGN KEY (tour_id) REFERENCES tour_dates(id)
);
