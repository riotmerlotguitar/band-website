# Table Seeds:
INSERT INTO tour_dates (tour_date, country, state, city, venue, price)
VALUES ('2019-05-01', 'United States', 'Massachusetts', 'Boston', 'House of Blues', 32.00),
       ('2019-05-12', 'United States', 'New York', 'New York', 'Madison Square Gardens', 45.00),
       ('2019-05-25', 'United States', 'Utah', 'Salt Lake City', 'The Complex', 45.00);

INSERT INTO merchandise_category (name)
VALUES ('Albums'),
       ('Swag'),
       ('Meet and Greet'),
       ('Season Pass');

INSERT INTO merchandise (name, price, image, category_id)
VALUES ('Merlot Rain Down', 12.00, '../../assets/images/albums/album.png', 1),
       ('Riot in My Bedroom', 13.25, '../../assets/images/albums/album.png', 1),
       ('My Guitar, My Life', 11.50, '../../assets/images/albums/album.png', 1),
       ('Merlot Drip', 12.50, '../../assets/images/albums/album.png', 1),
       ('Riot Till Death', 12.00, '../../assets/images/albums/album.png', 1),
       ('Leather Trucker Hat', 15.00, '../../assets/images/swag/trucker_hat.png', 2),
       ('Leather T-Shirt', 20.99, '../../assets/images/swag/t_shirt.png', 2),
       ('Leather Pants', 56.49, '../../assets/images/swag/pants.png', 2),
       ('Leather Gloves', 32.99, '../../assets/images/swag/trucker_hat.png', 2),
       ('Studded Shoulder Pads', 40.99, '../../assets/images/swag/pads.png', 2),
       ('5 Minute Meet & Greet with Band', 199.99, '../../assets/images/passes/meet_and_greet.png', 3),
       ('Lunch and a Movie with 1 Band Member', 599.99, '../../assets/images/passes/meet_and_greet.png', 3),
       ('Sit Down Dinner with the Drummer', 499.99, '../../assets/images/passes/meet_and_greet.png', 3),
       ('Couples Massage with the Bassist', 699.99, '../../assets/images/passes/meet_and_greet.png', 3),
       ('Sleep Over with Entire Band', 2999.99, '../../assets/images/passes/meet_and_greet.png', 3),
       ('Season Pass to All Shows', 899.99, '../../assets/images/passes/season_pass.png', 4);

INSERT INTO band_info (name, image, description)
VALUES ('Riot Merlot Guitar',
        '../../assets/images/band/group.png',
        'The Riot Merlot Guitar obtained platinum status after making comments at the
         2019 Billboard Music Awards about how Taylor Swift didn\'t deserve to win.'),
       ('Merp',
        '../../assets/images/band/merp.png',
        'Merp spends his time milking cows when not busy shredding on the guitar.'),
       ('Rhino',
        '../../assets/images/band/rhino.png',
        'Rhino volunteers at his local church on Sunday mornings and drinks lambs blood Sunday evening.'),
       ('Hog',
        '../../assets/images/band/hog.png',
        'Hog\'s drums are made out of human heads, so when he\'s tapping away a sick beat, his feet
        stay cold in the blood of rival bands.'),
       ('Spawn',
        '../../assets/images/band/spawn.png',
        'Spawn puts people to sleep with his melodic piano playing. Once asleep, he drinks their blood.'),
       ('Redeye',
        '../../assets/images/band/redeye.png',
        'Redeye plucks the bass with severed hands that fans throw on stage.');

INSERT INTO album_info (name, description, release_date, image)
VALUES ('Merlot Rain Down', '"So incredibly beautiful" - Rolling Stone, "All time favorite album" - Esquire -- Simply the best', '1999-04-20', '../../assets/images/albums/album.png'),
       ('Riot in My Bedroom', '"This puts my music to shame" - Marvin Gaye -- This album single handedly created an unprecedented boom in global population from 2002 to present day', '2001-02-14', '../../assets/images/albums/album.png'),
       ('My Guitar, My Life', '"F**k it, lets a take a road trip" - Pretty much all of America -- This album will get you to where you going faster than an F-35', '2004-07-09', '../../assets/images/albums/album.png'),
       ('Merlot Drip', '"The swag this album delivers is unmatched" - Jay Z -- They play this on repeat in the hood', '2005-08-11', '../../assets/images/albums/album.png'),
       ('Riot Till Death', '"Your face will melt off before you realize what is happening" - Some dudes skeleton -- The rawest of the Raw', '1996-01-01', '../../assets/images/albums/album.png');

INSERT INTO track_info (name, duration, album_id)
VALUES ('AHHHHH', '2:53', 5),
       ('Say Goodbye to Your Family', '2:23', 1),
       ('We are Incredible', '3:56', 3),
       ('Huge', '4:42', 2),
       ('Shred', '3:11', 5),
       ('Swiggity Swag', '2:49', 4),
       ('Lyrical Genius', '3:09', 1),
       ('I Married My Guitar', '2:30', 3),
       ('Im Spent', '3:01', 2),
       ('We are Dead', '2:59', 4);

INSERT INTO media (title, video, description)
VALUES ('Tears On My Merlot Guitar',
        '../../assets/videos/media/tears.png',
        'Billboard top 10 for 2019: Tears on My Merlot Guitar'),
       ('I can\'t hear you scream, try harder',
        '../../assets/videos/media/scream.png',
        'Bring out your animalistic side with this hit wonder.'),
       ('Bad Girls',
        '../../assets/videos/media/girls.png',
        'Every metal band has a song about bad girls.');

INSERT INTO news (title, image, description)
VALUES ('Cities on Fire after The Riot Merlot Guitar Tour.',
        '../../assets/images/news/city.png',
        'Cities that the Riot Merlot Guitar band visits is left ablaze.'),
       ('President Putin declares the First Day of February: Merlot Day.',
        '../../assets/images/news/putin.png',
        'Russian pres to commemorate the RMG - Feb 1 bottomless merlot.'),
       ('Ibanez Bankrupt! Riot Merlot Guitar Crushes Competition.',
        '../../assets/images/news/guitar.png',
        'Filing for CHP 7 bankruptcy. Reason? RMG guitars: Devil\'s Merlot');
